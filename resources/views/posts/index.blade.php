@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin())
  <div class="clearfix">
    <a href="{{ route('posts.create') }}"
    class="btn float-right btn-success"
    style="margin-bottom: 10px">
      Add Post
    </a>
  </div>
  <div class="card card-default">
    <div class="card-header">All Posts</div>
        @if ($posts->count() > 0)


        <div class="clearfix">
            <form   
            action="{{ route('import') }}"
             method="POST" 
               enctype="multipart/form-data">

               
               @csrf
                @method('POST')
                <input type="file" name="file" class="form-control">
                <br>
                <button class="btn btn-success">Import File</button>
            </form>
        </div>


          <table class="card-body">
            <table class="table">
              <thead>
                <tr>
                <th>Download</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($posts as $post)
                  <tr>
                  <td>
                  <a href="storage/{{$post->image}}" class="btn btn-secondary" download="{{$post->image}}">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-bar-down"
                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" 
                       d="M1 3.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5zM8 6a.5.5 
                       0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 
                       .708-.708L7.5 12.293V6.5A.5.5 0 0 1 8 6z"/>
                       </svg>
</a>
</td>
                    <td>
                      <img src="{{ asset('storage/'.$post->image) }}" alt="" width="100px" height="50px">
                    </td>
                    <td>
                      {{ $post->title }}
                    </td>
                    <td>
                      <form class="float-right ml-2"
                      action="{{route('posts.destroy', $post->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                          <button class="btn btn-danger btn-sm">
                            {{ $post->trashed() ? 'Delete' : 'Trash' }}
                        </button>
                      </form>
                      @if (!$post->trashed())
                        <a href="{{route('posts.edit', $post->id)}}" class="btn btn-primary float-right btn-sm">Edit</a>
                      @else
                        <a href="{{route('trashed.restore', $post->id)}}" class="btn btn-primary float-right btn-sm">Restore</a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        @else
          <div class="card-body">
            <h1 class="text-center">
              No Posts Yet.
            </h1>
          </div>
        @endif
    </div>
  </div>
  @endif
@endsection
