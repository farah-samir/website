@extends('layouts.app')

@section('content')

            <div class="container">
            <div class="row">
            <div class="col-md-6 offset-md-3">
          

        <div class="card-body">
            <form   
            action="{{ route('import') }}"
             method="POST" 
               enctype="multipart/form-data">

               
               @csrf
                @method('POST')
                <input type="file" name="file" class="form-control">
                <br>
                <button class="btn btn-success">Import File</button>
            </form>
        </div>




            </div>
            </div>
            </div>




@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.0/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>
      $(document).ready(function() {
        $('.tags').select2();
      });
    </script>
@endsection