<?php

namespace App\Http\Controllers;
use App\Post;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index() {
       // return view('news');
        return view('news')->with('posts', Post::all());

      }
}
